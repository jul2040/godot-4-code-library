extends TestGUI

func get_settings() -> Dictionary:
	return {
		"Movement":{
			"Acceleration":["accel", 0, 5000, "../.."],
			"Deceleration":["decel", 0, 5000, "../.."],
			"Air Acceleration Multiplier":["air_accel_mult", 0, 3.0, "../.."],
			"Air Deceleration Multipler":["air_decel_mult", 0, 3.0, "../.."],
			"Max Speed":["max_speed", 0, 4000, "../.."],
			"Floor Snap Length":["floor_snap_length", 0, 50, "../.."]
		},
		"Jump":{
			"Jump Velocity":["jump_velocity", 0, 2000, "../.."],
			"Coyote Time":["coyote_time", 0, 0.5, "../.."],
			"Jump Buffer":["jump_buffer", 0, 0.5, "../.."],
			"Jump Reverse Amount":["jump_reverse_amount", 0, 1000, "../.."],
			"Jump Reverse Max":["jump_reverse_max", 0, 2, "../.."],
			"Gravity":["gravity", 0, 5000, "../.."],
			"Down Gravity":["down_gravity", 0, 5000, "../.."],
			"Max Y Speed":["max_y_speed", 0, 3000, "../.."],
		},
		"Camera":{
			"Smoothing Speed":["smoothing_speed", 1, 30, "../../CameraController/Camera"],
			"Down Margin":["down_margin", 0, 300, "../../CameraController"],
			"Up Margin":["up_margin", 0, 300, "../../CameraController"],
			"Lookahead":["lookahead", 0, 1, "../../CameraController"],
		},
	}
