extends CharacterBody2D

signal touched_floor
signal left_floor

@export var accel:float = 1500
@export var decel:float = 3500
@export var air_accel_mult:float = 1.0
@export var air_decel_mult:float = 0.3
@export var max_speed:float = 900

@export var coyote_time:float = 0.2
@export var jump_buffer:float = 0.2

@export var jump_velocity:float = 800

@export var jump_reverse_amount:float = 600 # velocity impulse per second
@export var jump_reverse_max:float = 1.0

@export var gravity:float = 1000
@export var down_gravity:float = 2000
@export var max_y_speed:float = 1000

var coyote_timer:float = coyote_time
var jump_buffer_timer:float = jump_buffer
var jump_reverse_charge:float = 0

var was_on_floor:bool = false

func _process(delta):
	coyote_timer += delta
	coyote_timer = min(coyote_timer, coyote_time)
	jump_buffer_timer += delta
	jump_buffer_timer = clamp(jump_buffer_timer, 0, jump_buffer)

func _physics_process(delta:float):
	if(is_on_floor() and not was_on_floor):
		emit_signal("touched_floor")
	elif(not is_on_floor() and was_on_floor):
		emit_signal("left_floor")
	var right_direction:Vector2 = up_direction.rotated(PI/2)
	apply_movement(right_direction, delta)
	apply_max_speed(up_direction, right_direction)
	if(is_on_floor()):
		if(was_on_floor):
			coyote_timer = 0.0
			jump_reverse_charge = 0.0
		if(Input.is_action_just_pressed("jump") or (jump_buffer_timer < jump_buffer and Input.is_action_pressed("jump"))):
			jump()
			jump_buffer_timer = jump_buffer
	else:
		if(Input.is_action_just_pressed("jump")):
			if(coyote_timer < coyote_time):
				jump()
			else:
				jump_buffer_timer = 0.0
		if(Input.is_action_pressed("jump")):
			jump_reverse_charge += delta
			jump_reverse_charge = clamp(jump_reverse_charge, 0.0, jump_reverse_max)
		if(Input.is_action_just_released("jump")):
			jump_buffer_timer = jump_buffer
			jump_reverse()
	apply_gravity(-up_direction, delta)
	was_on_floor = is_on_floor()
	move_and_slide()

# jump, with an initial velocity
func jump():
	velocity.y = -jump_velocity
	coyote_timer = coyote_time

# called when you release the jump, gives the "variable jump" effect
# basically just adds to your velocity when you release
func jump_reverse():
	# if we are already moving down, don't move down
	if(velocity.y > 0):
		return
	velocity.y += (jump_reverse_max-jump_reverse_charge)*jump_reverse_amount
	jump_reverse_charge = jump_reverse_max

# applies gravity in a particular direction
# direction will not be down if we are standing on a slope
func apply_gravity(direction:Vector2, delta:float):
	var g:float = gravity
	if(velocity.y > 0):
		g = down_gravity
	velocity += direction*(delta*g)

# gets the up direction
# if we are on a slope, the up direction faces away from the slope
func get_up_dir():
	var up:Vector2 = Vector2.UP
	if(is_on_floor()):
		for i in get_slide_collision_count():
			var col:KinematicCollision2D = get_slide_collision(i)
			if(col.get_normal().angle_to(Vector2.UP) < PI/4):
				up = col.get_normal()
	up_direction = up

# applies the max speed
func apply_max_speed(up:Vector2, right:Vector2):
	# apply max speed in the right direction
	velocity = velocity.project(up)+(velocity.project(right).limit_length(max_speed))
	# apply max speed in the up direction
	velocity = velocity.project(right)+(velocity.project(up).limit_length(max_y_speed))

# applies movement based on the input
func apply_movement(right:Vector2, delta:float):
	var inp:float = Input.get_axis("left", "right")
	var a:float = accel
	var d:float = decel
	if(not is_on_floor()):
		a *= air_accel_mult
		d *= air_decel_mult
	# apply acceleration
	if(inp != 0):
		velocity += right*(inp*delta*a)
	# apply deceleration
	if(sign(velocity.x) != sign(inp)):
		velocity -= right*(clamp((delta*d), 0, velocity.project(right).length())*sign(velocity.x))
