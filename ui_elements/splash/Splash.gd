extends TextureRect

@export var skippable:bool = true
@export var grow_time:float = 1.0
@export var rest_time:float = 1.5
@export var skip_rest_time:float = 0.25
@export var grow_size:float = 1.0
@export var rest_size:float = 0.9

signal finished

func _on_splash_resized():
	pivot_offset = size/2

@onready var tween:Tween = get_tree().create_tween()
func _ready():
	modulate = Color(1.0, 1.0, 1.0, 0.0)
	tween.set_trans(Tween.TRANS_QUAD).set_ease(Tween.EASE_OUT)
	tween.tween_property(self, "scale", Vector2(grow_size, grow_size), grow_time)
	tween.parallel().tween_property(self, "modulate", Color(1.0, 1.0, 1.0, 1.0), grow_time)
	tween.chain().tween_property(self, "scale", Vector2(rest_size, rest_size), rest_time)
	tween.parallel().tween_property(self, "modulate", Color(1.0, 1.0, 1.0, 0.0), rest_time)
	await tween.finished
	emit_signal("finished")
	queue_free()

func _input(event:InputEvent):
	if(time_since_start < rest_time+grow_time-skip_rest_time and skippable and event.is_action_pressed("ui_accept")):
		tween.kill()
		tween = get_tree().create_tween()
		tween.chain().tween_property(self, "scale", Vector2(rest_size, rest_size), skip_rest_time)
		tween.parallel().tween_property(self, "modulate", Color(1.0, 1.0, 1.0, 0.0), skip_rest_time)
		await tween.finished
		emit_signal("finished")
		queue_free()

var time_since_start:float = 0.0
func _process(delta:float):
	time_since_start += delta
