extends Control

@export var inbetween_time:float = 0.5
@export var skippable:bool = true
@export var images:Array[Texture]
@export var next_scene:PackedScene = null

signal finished

func _ready():
	var i = 0
	for image in images:
		var splash = preload("Splash.tscn").instantiate()
		splash.skippable = skippable
		splash.texture = image
		add_child(splash)
		await splash.finished
		if(i != len(images)-1):
			# only wait if this isnt the last splash
			await get_tree().create_timer(inbetween_time).timeout
		i += 1
	emit_signal("finished")
	if(next_scene == null):
		queue_free()
	else:
		TransitionManager.change_scene_to(next_scene)
