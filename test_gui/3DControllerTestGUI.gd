extends TestGUI

func get_settings() -> Dictionary:
	return {
		"Physics":{
			"Floor Acceleration":["floor_accel", 0.0, 30.0, "../.."],
			"Air Acceleration":["air_accel", 0.0, 30.0, "../.."],
			"Floor Friction":["floor_friction", 0.0, 1.0, "../.."],
			"Max Speed":["max_speed", 0.0, 1000.0, "../.."],
			"Jump Velocity":["jump_velocity", 0.0, 100.0, "../.."]
		},
		"Camera Damping":{
			"Frequency":["f", 0.1, 50.0, "../../Damper3D"],
			"Damping":["z", 0.1, 50.0, "../../Damper3D"],
			"Response":["r", -50.0, 50.0, "../../Damper3D"],
		},
		"Camera":{
			"Mouse Sensitivity":["mouse_sensitivity", 0.001, 0.05, "../../Damper3D/CameraArm"],
			"Max Zoom":["max_distance", 0.0, 50.0, "../../Damper3D/CameraArm"],
			"Min Zoom":["min_distance", 0.0, 50.0, "../../Damper3D/CameraArm"],
			"Zoom Interval":["zoom_interval", 0.0, 3.0, "../../Damper3D/CameraArm"],
			"Zoom Time":["zoom_time", 0.0, 3.0, "../../Damper3D/CameraArm"],
		},
	}
