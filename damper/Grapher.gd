extends Line2D
class_name Grapher

# this node graphs the currently selected damping settings

@export var x_scale:float = 2.0
@export var y_scale:float = 0.5
@export var time_step:float = 0.1
@export var step_count:int = 400

@onready var damper:Damper = $Damper

# gets called whenever the user changes the damping settings
func update_graph(f:float, z:float, r:float):
	damper.f = f
	damper.z = z
	damper.r = r
	damper.reset()
	clear_points()
	for i in step_count:
		var x:float = -input(float(i)/float(step_count))*step_count*y_scale
		add_point(Vector2(i*x_scale, damper.update_damper(time_step, x)))

@onready var base:Line2D = $Base

func _ready():
	# graph the red line
	update_graph(damper.f, damper.z, damper.r)
	# graph the white line
	for i in step_count:
		var x = -input(float(i)/float(step_count))*step_count*y_scale
		base.add_point(Vector2(i*x_scale, x))

# defines the curve used for the white line
func input(x:float) -> float:
	if(x < 0.25):
		return 0.0
	elif(x < 0.5):
		return 1.0
	elif(x < 0.75):
		return 4*x-2
	else:
		return -4*x+4
